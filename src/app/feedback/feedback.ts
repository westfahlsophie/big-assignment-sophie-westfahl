export class Feedback {

    name: string;
    email: string;
    scale: number;
    comment: string;

    constructor(newName: string, newEmail: string, newScale: number, newComment: string){ 
        this.name = newName;
        this.email = newEmail;
        this.scale = newScale;
        this.comment = newComment;

    }
}


import { Component, OnInit } from '@angular/core';
import { Cloth } from '../cloth/cloth';
import { ClothService } from '../cloth/cloth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-products',
  templateUrl: 'products.page.html',
  styleUrls: ['products.page.scss'],
})
export class ProductsPage implements OnInit {

  items = Array<Cloth>();

  constructor( private clothService: ClothService, private router: Router) {
    
  }

  ngOnInit() {
    this.items = this.clothService.items;
    console.log(this.clothService.items);
  }

  openDetails(item){
    this.clothService.items = item;
    this.router.navigate(['../productdetails']);
  }

}
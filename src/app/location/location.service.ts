import { Injectable } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  constructor(private geolocation: Geolocation, private http: HttpClient) { }

  /**
   * Get current location (coordinates).
   */
  public getLocation() {
    return this.geolocation.getCurrentPosition();
  }

  /**
   *  Get changes on position.
   */
  public watchPosition() {
    return this.geolocation.watchPosition();
  }

  /**
   * Translate coordinates into address by using OpenGage Geocoding API.
   * 
   * @param lat Latitude used to retrieve address.
   * @param lon Longitude used to retrieve address.
   */
  public getLocationName(lat: number, lon: number): Observable<any> {
    // DO NOT USE MY APIKEY, IF USING SERVICE FROM OPENGAGE!!!
    const address = environment.geocodingUrl + 'q=' + lat + '+' + lon + '&key=' + environment.geocodingApiKey;
    return this.http.get(address);
  }
}

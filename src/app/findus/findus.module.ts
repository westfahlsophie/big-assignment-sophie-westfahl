import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FindusPageRoutingModule } from './findus-routing.module';

import { FindusPage } from './findus.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FindusPageRoutingModule
  ],
  declarations: [FindusPage]
})
export class FindusPageModule {}

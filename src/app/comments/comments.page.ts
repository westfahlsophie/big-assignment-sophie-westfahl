import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Feedback } from '../feedback/feedback';
import { Storage } from '@ionic/storage';
import { CommentingPage } from '../commenting/commenting.page'


@Component({
  selector: 'app-comments',
  templateUrl: './comments.page.html',
  styleUrls: ['./comments.page.scss'],
})
export class CommentsPage implements OnInit {

  feedbacks= Array<Feedback>();

  constructor(private router: Router, private commentingPage: CommentingPage, private storage: Storage) { 
    this.storage.get('feedback').then((val) => {
      this.feedbacks = JSON.parse(val);      

      if(!this.feedbacks){
        this.feedbacks = [];
      }
    });
  } 

  ngOnInit() {
    this.storage.get('feedback').then((val) => {
      this.feedbacks = JSON.parse(val);      

      if(!this.feedbacks){
        this.feedbacks = [];
      }

    });

    console.log(this.storage);
  }

  openCreateComment(){
    this.router.navigate(['../commenting']);
  }

  clearStorage(){
    this.storage.clear();
  }

  doRefresh(event) {
    console.log('Begin async operation');

    this.storage.get('feedback').then((val) => {
      this.feedbacks = JSON.parse(val);      

      if(!this.feedbacks){
        this.feedbacks = [];
      }

    });
    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }
}

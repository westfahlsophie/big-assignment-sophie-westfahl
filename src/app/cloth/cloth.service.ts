import { Injectable } from '@angular/core';
import { Cloth } from './cloth';

@Injectable({
  providedIn: 'root'
})
export class ClothService {

  items= Array<Cloth>();

  constructor() { 
    this.items.push(new Cloth ('../../assets/christmastree.png','Christmastree', 'Christmastree made out of woodensticks with lights. Dekoration not included.', 25.00));
    this.items.push(new Cloth ('../../assets/cloathook.png','Cloth hook', 'SCloat hook made out of wooden sticks. Size optional. Grinded; no destroying of cloth.', 23.00));
    this.items.push(new Cloth ('../../assets/closet.png','Industrial Storage Rack', 'Perfect for jackets, dresses or bags. Individual arrangement of metal rods.', 60.00));
    this.items.push(new Cloth ('../../assets/light.png','Spectacular Floor Log Lamp', 'Made out of beech wood. Height individually.', 30.00));
    this.items.push(new Cloth ('../../assets/toiletpaper.png','Cloud concrete Toilet roll holder', 'Made out of concrete for more than 11 rolls.', 29.00));
    this.items.push(new Cloth ('../../assets/tvbar.png','TV Cabinet ', 'Cinder blocks and recycled timber. Height and length individually selectable.', 42.00));
    this.items.push(new Cloth ('../../assets/tvdesk.png','Concrete Table', 'Perfect for your living room. 2 levels possible for Magazines, etc. Concrete and beech.', 59.00));
    

  }

}
